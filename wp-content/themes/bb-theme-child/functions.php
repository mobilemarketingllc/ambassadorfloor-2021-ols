<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';


add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
	wp_enqueue_script("PDF_script","https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js","","",1);
    wp_enqueue_script("shareBox-script", get_stylesheet_directory_uri()."/resources/sharebox/needsharebutton.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);

});


// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


add_filter('gform_form_args', 'no_ajax_on_all_forms', 10, 1);
function no_ajax_on_all_forms($args){
    $args['ajax'] = false;
    return $args;
}

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );





add_action( 'wp_ajax_nopriv_add_fav_product', 'add_fav_product' );
add_action( 'wp_ajax_add_fav_product', 'add_fav_product' );

function add_fav_product() {
    $is_fav = $_POST['is_fav'];
    $post_id = $_POST['post_id'];

    $result = update_post_meta( $post_id, 'is_fav', $is_fav);
if($result == false){
    $result1 = add_post_meta( $post_id, 'is_fav', $is_fav);
}
}

add_action( 'wp_ajax_nopriv_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );
add_action( 'wp_ajax_base64_to_jpeg_convert', 'base64_to_jpeg_convert' );

function base64_to_jpeg_convert() {    
    global $wpdb;

    $upload_dir = wp_get_upload_dir();
    $output_file= $upload_dir['basedir']. '/measure/'.uniqid().'.png';

    $img = $_POST['imagedata']; 
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    file_put_contents($output_file, $data);
    
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'userid' => $current_user->ID, 
            'image_name' => $_POST['imagename'],
            'image_path' => $output_file 
        );    
    
         $wpdb->insert( 'wp_measure_images', $data);

         $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="javascript:void(0)" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;


        wp_die();
    }

}

function measurement_tool_images($arg){    
    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';
        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $image_url = home_url().''.$img_path;

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="javascript:void(0);" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button view" href="javascript:void(0);" title="#"  data-img="'.home_url().''.$img_path.'" onclick="openPopUp(this)" data-title="'.$img->image_name.'">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";
        

        return $content;
    }
   
  
}    
add_shortcode('measurementtool_images', 'measurement_tool_images');


add_action( 'wp_ajax_nopriv_delete_measureimg', 'delete_measurement_images' );
add_action( 'wp_ajax_delete_measureimg', 'delete_measurement_images' );

function delete_measurement_images() {  
    global $wpdb;

    $mid = $_POST['mimg_id'];

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;

        $wpdb->get_results('DELETE FROM wp_measure_images WHERE userid = '.$current_user->ID.' and id = '.$mid.'');
        

        $measureimages = $wpdb->get_results("SELECT * FROM wp_measure_images WHERE userid = $current_user->ID ");

        write_log($measureimages);

        
        $content = '<div id="mesureMentprintMe" class="product-plp-grid product-grid swatch "><div class="row product-row" id="aniimated-thumbnials">';

        $content .= '<h3 style="text-align: center; font-size: 33px;"><span>SAVED MEASUREMENTS</span></h3>';

        foreach($measureimages as $img){

           

            $img_path = str_replace('/var/www/html', '', $img->image_path);

            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap mesureMentWrap">';

            $content .= '<a href="'.home_url().''.$img_path.'" title="#" class="wplightbox"><img class="measure_img" src="'.home_url().''.$img_path.'" /></a>';

            $content .= '<div class="favButtons button-wrapper">
                <a class="button fl-button" href="javascript:void(0)" title="#" onclick="openPopUp(this)"   data-img="'.home_url().''.$img_path.'" data-title="'.$img->image_name.'" onclik="openPopUp(e)">VIEW</a>';
                $content .= '<a class="button fl-button deletemeasure" href="#" data-id="'.$img->id.'"><i class="fa fa-trash" aria-hidden="true"></i></a>'; 
                $content .= '</div> </div>';
                $content .= '<h3><span>'.$img->image_name.'</span></h3>';

                $content .= "</div></div>";

        }

        $content .= "</div></div>";

        echo $content;
    }

  wp_die();

}

add_action( 'wp_ajax_nopriv_add_favroiute', 'add_favroiute' );
add_action( 'wp_ajax_add_favroiute', 'add_favroiute' );

function add_favroiute() {

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $current_user->ID, 
            'product_id' => $_POST['post_id']           
        );    
    
         $wpdb->insert( 'wp_favorite_posts', $data);        

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }
}

add_action( 'wp_ajax_nopriv_remove_favroiute', 'remove_favroiute' );
add_action( 'wp_ajax_remove_favroiute', 'remove_favroiute' );

function remove_favroiute() {    

    global $wpdb;

    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        write_log( 'Personal Message For '. $current_user->ID ) ;
     
        $data=array(
            'user_id' => $_POST['user_id'], 
            'product_id' => $_POST['post_id']           
        );    
    
        $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE user_id = '.$_POST['user_id'].' and product_id = '.$_POST['post_id'].'');

         return true;

         wp_die();

    }else{

        return false;

        wp_die();
    }

}

// Favorite product shortcodes 
function greatcustom_favorite_posts_function(){

    global $wpdb;
    $content = "";
    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog', 'instock_laminate');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Instock Laminate"=>"instock_laminate",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div id="ajaxreplace"><div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare" data-html2canvas-ignore>
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        $content .= '<div class="printthisDiv">';
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            // write_log('check_note');
            // write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img crossorigin="Anonymous"  class="list-pro-image" src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        // $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }
        echo '</div></div>';
		$content .= '</div>';
 }  else {
  		 $content .= ' <a class="fl-button addFavProductButton" href="/flooring/carpet/products/">Add Favorite Products</a>';
}
		  
return $content;
	
}
add_shortcode('greatcustom_favorite_posts', 'greatcustom_favorite_posts_function');


add_action( 'wp_ajax_nopriv_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
add_action( 'wp_ajax_remove_favroiute_list', 'remove_greatcustom_favorite_posts_function' );
function remove_greatcustom_favorite_posts_function(){

    global $wpdb;

    $content = "";

    $wpdb->get_results('DELETE FROM wp_favorite_posts WHERE product_id = '.$_POST['post_id'].' and user_id = '.get_current_user_id().'');    

    $fav_sql = 'SELECT product_id FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().'';
            
    $check_fav = $wpdb->get_results($fav_sql);    

    write_log($check_fav);

    $fav_products = array();

    foreach($check_fav as $fav){

        $fav_products[] = $fav->product_id;
    }

    write_log('dsfsd'.$fav_products);
    
    $favorite_post_ids = array_reverse($fav_products);
    $page = intval(get_query_var('paged'));

    $post_array = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');

        $brandmapping = array(
            "Carpet"=>"carpeting",
            "Hardwood"=>"hardwood_catalog",
            "Laminate"=>"laminate_catalog",
            "Luxury Vinyl Tile"=>"luxury_vinyl_tile",
            "Ceramic Tile"=>"tile_catalog",
            "Waterproof"=>"solid_wpc_waterproof"
        );
    

   

    $content .= '<div class="favProWarpper printWrap">
                    <div class="shareAndPrint hideShare" data-html2canvas-ignore>
                        <a href="#"  id="share-button-3" class="need-share-button-default" data-share-position="middleBottom" data-share-networks="Mailto,Twitter,Pinterest,Facebook,GooglePlus,Linkedin"></a>
                        <a href="#" class="printDOc" ><i class="fa fa-print" aria-hidden="true"></i></a>
                    </div>
                <div class="row statiContent">
                    <div class="col-lg-12">
                        <h2>MY FAVORITE PRODUCTS </h2>
                        <p>Our product catalog is home to thousands of products. 
                        Collect your favorites and save them here to help you select the right product for your home.</p>
                    </div>
                </div>';
    
    if(!empty($check_fav)) {

        
        foreach($post_array as $posttype){

            $qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
           //  $qry['post_type'] = array('carpeting','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','tile_catalog');
           $qry['post_type'] = array($posttype);
           
           query_posts($qry);
    
           $post_type_name = array_search($posttype,$brandmapping);
            
           $content .= '<div id="printMe" class="product-plp-grid product-grid swatch "><div class="row product-row">';
            if ( have_posts() ) :
                $content .= '<h3>'.$post_type_name.'</h3>';
            endif;
            while ( have_posts() ) : the_post();

            $note_sql = 'SELECT note FROM wp_favorite_posts WHERE user_id = '.get_current_user_id().' and product_id='.get_the_ID().'';
            
            $check_note = $wpdb->get_results($note_sql, ARRAY_A);  
            write_log('check_note');
            write_log($check_note[0]['note']);
               
            $content .= '<div class="col-lg-4 col-md-4 col-sm-6 "><div class="fl-post-grid-post">';
            $content .= '<div class="fl-post-grid-image prod_like_wrap">';
    
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
    
                $sku = get_post_meta(get_the_ID(),'sku',true);
                $manufacturer = get_post_meta(get_the_ID(),'manufacturer',true);
    
                $content .= '<a href="'.get_permalink().'" title="'.get_the_title() .'"><img class="list-pro-image" crossorigin="Anonymous"  src="'. $image.'" alt="'.get_the_title().'" /></a>';
    
                $content .= '<div class="favButtons button-wrapper">
                    <a class="button fl-button" href="'.get_permalink().'" title="'.get_the_title() .'">VIEW PRODUCT</a>';
                   // $content .=   roomvo_script_integration($manufacturer,$sku,get_the_ID());
                   if($check_note[0]['note'] == '' || $check_note[0]['note'] == null){
                    $content .= '<a class="button fl-button add_note_fav" id="add_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'">ADD A NOTE</a>'; 
                   }
                    $content .= '<div class="buttonHolder"><a id="rem_fav" data-id="'.get_the_ID().'" class="remove-parent" data-user_id="'.get_current_user_id().'" href="javascript:void(0)"><i class="fa fa-trash" aria-hidden="true"></i></a>';

                    if($check_note[0]['note'] != '' || $check_note[0]['note'] != null){
                        $content .= '<a class="view_note_fav" data-note="'.$check_note[0]['note'].'" id="view_note_fun" href="javascript:void(0)" data-productid="'.get_the_ID().'"><i class="fa fa-sticky-note" aria-hidden="true"></i></a>'; 
                        $content .= '<div id="'.get_the_ID().'" style="display:none">'.$check_note[0]['note'].'</div>';
                       }
                    $content .= '</div></div> </div>';
                    $content .= ' <div class="fl-post-grid-text product-grid btn-grey">';
                    $content .= '<h4><span>'.get_the_title().'</span></h4>';
                    $content .= '</div>';
    
                //wpfp_remove_favorite_link(get_the_ID());
                $content .= "</div></div>";
            endwhile;
            $content .= "</div></div>";
        
    
        }

        $content .='</div></div>';
        

    echo $content;

    wp_die();

 }

}

add_action('wp_ajax_add_note_form', 'add_note_form');
add_action('wp_ajax_nopriv_add_note_form', 'add_note_form');

function add_note_form()
{
    global $wpdb;

    $fav_sql = 'UPDATE wp_favorite_posts SET note = "'.$_POST['note'].'" WHERE user_id = '.get_current_user_id().' and product_id = '.$_POST['addnote_productid'].'';
            
    $check_fav = $wpdb->get_results($fav_sql); 

    echo $fav_sql;
}


add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}



//check out flooring FUnctionality

add_action( 'wp_ajax_nopriv_check_out_flooring', 'check_out_flooring' );
add_action( 'wp_ajax_check_out_flooring', 'check_out_flooring' );

function check_out_flooring() {

    $arg = array();
    if($_POST['product']=='carpeting'){

        $posttype = array('carpeting');

    }elseif($_POST['product']=='hardwood_catalog,laminate_catalog,luxury_vinyl_tile'){

        $posttype = array('hardwood_catalog','laminate_catalog','luxury_vinyl_tile');

    }elseif($_POST['product']=='tile_catalog' || $_POST['product']=='tile'){

        // $posttype = array('tile_catalog','hardwood_catalog','laminate_catalog','luxury_vinyl_tile','carpeting');
        $posttype = array('tile_catalog');
    }

    $parameters =  explode(",",$_POST['imp_thing']);

foreach($parameters as $para){
     if($para == 'hypoallergenic'){

        $arg['hypoallergenic'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50
        );
     }elseif($para == 'diy_friendly'){

        $arg['diy_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'installation_facet',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'installation_method',
                    'value'   => 'Locking',
                    'compare' => '=',
                    
                ),
            ),             
        );
     }elseif($para == 'aesthetic_beauty'){

        $arg['aesthetic_beauty'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'brand',
                    'value'   => 'Anderson Tuftex',
                    'compare' => '=',
                    
                ),
                array(
                    'key'     => 'brand',
                    'value'   => 'Karastan',
                    'compare' => '=',
                    
                ),
            ),
        );
     }elseif($para == 'eco_friendly'){
        $arg['eco_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Eco',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );
     }
     elseif($para == 'pet_friendly'){

        $arg['pet_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Pet',
                    'compare' => 'LIKE',
                    
                ),
            ),
        );

        
     }elseif($para == 'easy_to_clean'){
        
        $arg['easy_to_clean']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'durability'){
        
        $arg['durability']=array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }
     elseif($para == ''){
        
        $arg['colors']=array(
            'post_type'      => array( 'luxury_vinyl_tile', 'laminate_catalog','hardwood_catalog','carpeting','tile' ),
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
        );

        
     }
     elseif($para == 'stain_resistant'){
        
        $arg['stain_resistant'] =array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                'relation' => 'OR',
                array(
                    'key'     => 'lifestyle',
                    'value'   => 'Stain Resistant',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'R2x',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'Stainmaster',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'fiber',
                    'value'   => 'SmartStrand',
                    'compare' => 'Like',
                    
                ),
                array(
                    'key'     => 'Collection',
                    'value'   => 'Bellera',
                    'compare' => 'Like',
                    
                ),
            ),
        );

        
     }elseif($para == 'budget_friendly'){
        
        $arg['budget_friendly'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50,
            'meta_query'     => array(
                array(
                    'key'     => 'fiber',
                    'value'   => array( 'SmartStrand','Stainmaster','Bellera','Pet R2x' ),
                    'compare' => 'IN',
                    
                ),
            ),
        );

        
     }elseif($para == 'color'){
        
        $arg['color'] = array(
            'post_type'      => $posttype,
            'post_status'    => 'publish',
            'orderby'        => 'rand',
            'posts_per_page' => 50            
        );

        
     }

}
 
     $i = 1;

     $find_posts = array();
     foreach($parameters as $para){
        $wp_query[$i] = new WP_Query($arg[$para]);
        
        
        $find_posts = array_merge( $find_posts , $wp_query[$i]->posts);        

        $i++;
     }
     
     //print_r($find_posts);
     if (!$find_posts ) {
       
            $args =array(
                'post_type'      => $posttype,
                'post_status'    => 'publish',
                'posts_per_page' => 50,
                'orderby'        => 'rand'
            );
            $find_posts   = new WP_Query($args);
           // var_dump($args , $find_posts);
            $find_posts = $find_posts->posts;
     }

     if ( $find_posts ) {
          
        $content ="";

        foreach ( $find_posts as $post ) {

            $image = swatch_image_product_thumbnail($post->ID,'222','222');
            $sku = get_field( "sku", $post->ID );
           
     $content .= '<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 singlePro ">
                        <div class="card">
                            <div class="productImgWrap">
                                <img src="'.$image.'" alt="Product Image" style="width:100%!important">
                            </div>
                            <a href="javascript:void(0)" class="closebtn"  >&times;</a>
                            <div class="pro_title">
                                <h4>'.get_field( "collection", $post->ID ).'</h4> 
                                <h3>'.get_field( "color", $post->ID ).'</h3>
                                <p class="brand"><small><i>'.get_field( "brand", $post->ID ).'</i></small></p>
                                <!-- <a class="orderSamplebtn fl-button btnAddAction cart-action"  href="javascript:void(0)">
                                Order sample
                                </a> -->
                                <a class="prodLink" href="'.get_permalink($post->ID).'">See info</a>
                            </div>
                            
                        </div>
                    </div>';
        }

        $content .='<div class="text-center buttonWrap" data-search="'.$_POST['imp_thing'].'">
                    <a class="button" href="javascript:void(0)">view all results</a>
                    <a class="restartQuiz prodLink" href="javascript:void(0)" onclick="location.reload();">Restart Quiz</a>
                </div>';

    }

    echo $content;
    wp_die();
}


// IP location FUnctionality
  if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
    wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location(){

          global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

             // write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['name']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                //  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}




//get ipaddress of visitor

function getUserIpAddr() {
  $ipaddress = '';
  if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
  else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
  else
      $ipaddress = 'UNKNOWN';
  
  if ( strstr($ipaddress, ',') ) {
          $tmp = explode(',', $ipaddress,2);
          $ipaddress = trim($tmp[1]);
  }
  return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
  
  global $wpdb;
  $content="";
  
  $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';

  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];
      if(isset($_COOKIE['preferred_store'])){

          $store_location = $_COOKIE['preferred_store'];
      }else{

          $store_location = '';
         
        }      
      
      $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

           

      $storeposts = $wpdb->get_results($sql);
      $storeposts_array = json_decode(json_encode($storeposts), true);      

     
      if($store_location ==''){
              
          $store_location = $storeposts_array['0']['ID'];
          $store_distance = round($storeposts_array['0']['distance'],1);
      }else{

      
          $key = array_search($store_location, array_column($storeposts_array, 'ID'));
          $store_distance = round($storeposts_array[$key]['distance'],1);           

      }
      
    
      $content = get_the_title($store_location); 
      $phone = get_field('phone', $store_location); 


  foreach ( $storeposts as $post ) {
$content_list .= '<div class=" store_wrapper" id ="'.$post->ID.'">
                
                    <h5 class="title-prefix">'.get_the_title($post->ID).'</h5>
                    <h5 class="store-add"> '.get_field('address', $post->ID).'<br />'.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</h5>';
                  

                    if($post->ID != '1143810'){
                       
                        if(get_field(strtolower(date("l")), $post->ID) == 'CLOSED'){

                            $content_list .= '<p class="store-hour">CLOSED TODAY</p>';   
    
                            }else{

                              $openuntil = explode("-",get_field(strtolower(date("l")), $post->ID));
    
                            $content_list .= '<p class="store-hour">OPEN UNTIL '.str_replace(' ', '', $openuntil[1]).'</p>';

    
                            }

                    }else{

                        $content_list .='<p class="store-hour"><span class="">Not a Showroom (office only) </p>';

                    }                  
                    $content_list .='<p class="store-phone"><a href="tel:'.get_field('phone', $post->ID).'">'.get_field('phone', $post->ID).'</a> </p>'; 
                    if($post->ID != '1143810'){
                        
                        $content_list .='<a href="javascript:void(0)" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-distance="'.round($post->distance,1).'" data-phone="'.get_field('phone', $post->ID).'" target="_self" class="store-cta-link choose_location">Choose Location</a>';
                        $content_list .='<a href="'.get_field('store_url',$post->ID).'" target="_self" class="store-cta-link view_location"> View Location</a>';
                    }
                   
                    $content_list .='</div>'; 
  } 
  

  $data = array();

  $data['header']= $content;
  $data['phone']= $phone;
  $data['list']= $content_list;

  echo json_encode($data);
      wp_die();
}


add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );



//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {     
      
      $content = get_the_title($_POST['store_id']) ;      

      $content = '<div class="store_wrapper">';
      $content .='<div class="locationWrapFlyer"><div class="contentFlyer"><p>You re Shopping</p>
      <h3 class="header_location_name">'. get_the_title($_POST['store_id']) . '</h3>
      <div class="mobile"><a href="tel:'.$_POST['phone'].'">'.$_POST['phone'].'</a></div></div><div class="dropIcon"></div></div></div>';

      echo $content;

  wp_die();
}


/* Populate store location  */

add_filter( 'gform_pre_render_4', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_4', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_4', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_4', 'populate_product_location_form' );

add_filter( 'gform_pre_render_42', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_42', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_42', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_42', 'populate_product_location_form' );

add_filter( 'gform_pre_render_28', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_28', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_28', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_28', 'populate_product_location_form' );

add_filter( 'gform_pre_render_16', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_16', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_16', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_16', 'populate_product_location_form' );


add_filter( 'gform_pre_render_29', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_29', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_29', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_29', 'populate_product_location_form' );

add_filter( 'gform_pre_render_30', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_30', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_30', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_30', 'populate_product_location_form' );

add_filter( 'gform_pre_render_40', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_40', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_40', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_40', 'populate_product_location_form' );

add_filter( 'gform_pre_render_31', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_31', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_31', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_31', 'populate_product_location_form' );

add_filter( 'gform_pre_render_23', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_23', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_23', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_23', 'populate_product_location_form' );

add_filter( 'gform_pre_render_41', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_41', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_41', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_41', 'populate_product_location_form' );

add_filter( 'gform_pre_render_32', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_32', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_32', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_32', 'populate_product_location_form' );

add_filter( 'gform_pre_render_20', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_20', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_20', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_20', 'populate_product_location_form' );

add_filter( 'gform_pre_render_26', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_26', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_26', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_26', 'populate_product_location_form' );

add_filter( 'gform_pre_render_33', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_33', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_33', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_33', 'populate_product_location_form' );

add_filter( 'gform_pre_render_22', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_22', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_22', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_22', 'populate_product_location_form' );

add_filter( 'gform_pre_render_2', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_2', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_2', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_2', 'populate_product_location_form' );

add_filter( 'gform_pre_render_6', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_6', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_6', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_6', 'populate_product_location_form' );

add_filter( 'gform_pre_render_18', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_18', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_18', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_18', 'populate_product_location_form' );

add_filter( 'gform_pre_render_34', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_34', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_34', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_34', 'populate_product_location_form' );

add_filter( 'gform_pre_render_35', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_35', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_35', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_35', 'populate_product_location_form' );

add_filter( 'gform_pre_render_36', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_36', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_36', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_36', 'populate_product_location_form' );

add_filter( 'gform_pre_render_21', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_21', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_21', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_21', 'populate_product_location_form' );

add_filter( 'gform_pre_render_24', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_24', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_24', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_24', 'populate_product_location_form' );

add_filter( 'gform_pre_render_37', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_37', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_37', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_37', 'populate_product_location_form' );

add_filter( 'gform_pre_render_17', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_17', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_17', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_17', 'populate_product_location_form' );

add_filter( 'gform_pre_render_38', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_38', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_38', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_38', 'populate_product_location_form' );

add_filter( 'gform_pre_render_27', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_27', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_27', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_27', 'populate_product_location_form' );


add_filter( 'gform_pre_render_11', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_11', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_11', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_11', 'populate_product_location_form' );

add_filter( 'gform_pre_render_39', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_39', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_39', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_39', 'populate_product_location_form' );

add_filter( 'gform_pre_render_12', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_12', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_12', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_12', 'populate_product_location_form' );

add_filter( 'gform_pre_render_19', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_19', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_19', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_19', 'populate_product_location_form' );

add_filter( 'gform_pre_render_14', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_14', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_14', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_14', 'populate_product_location_form' );


add_filter( 'gform_pre_render_10', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_10', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_10', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_10', 'populate_product_location_form' );

add_filter( 'gform_pre_render_25', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_25', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_25', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_25', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

foreach ( $form['fields'] as &$field ) {

    // Only populate field ID 12
    if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
        continue;
    }      	

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );										
      
         $locations =  get_posts( $args );

         $choices = array(); 

         foreach($locations as $location) {
            

              $title = get_the_title($location->ID);
              
                   $choices[] = array( 'text' => $title, 'value' => $title );

          }
          wp_reset_postdata();

         // write_log($choices);

         // Set placeholder text for dropdown
         $field->placeholder = '-- Choose Location --';

         // Set choices from array of ACF values
         $field->choices = $choices;
    
}
return $form;
}


//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;
    $instock = get_post_meta( $post->ID , "in_stock");

    if ( is_singular( 'hardwood_catalog' )  ) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-hardwood-products/',
                'text' => 'In Stock Hardwood Products',
            );
        }else{
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/hardwood/',
                'text' => 'About Hardwood',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/hardwood/products/',
                'text' => 'Hardwood Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-carpet-products/',
                'text' => 'In Stock Carpet Products',
            );
        }else{    
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/carpet/',
                'text' => 'About Carpet',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/carpet/products/',
                'text' => 'Carpet Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-waterproof-luxury-vinyl-products/',
                'text' => 'In Stock Waterproof Luxury Vinyl Products',
            );
        }else{    
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/vinyl/',
                'text' => 'About Vinyl',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/vinyl/products/',
                'text' => 'Vinyl Products',
            );
        }
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-laminate-products/',
                'text' => 'In Stock Laminate Products',
            );
        }else{
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/laminate/',
                'text' => 'About Laminate',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/laminate/products/',
                'text' => 'Laminate Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {
        if($instock[0] == "1"){
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/',
                'text' => 'In Stock',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/in-stock/in-stock-tile-products/',
                'text' => ' In Stock Tile Products',
            );
        }else{
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/',
                'text' => 'Flooring',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/tile/',
                'text' => 'About Tile',
            );
            $breadcrumb[] = array(
                'url' => get_site_url().'/flooring/tile/products/',
                'text' => 'Tile Products',
            );
        }    
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    return $links;
}
